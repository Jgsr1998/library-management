# Use the official Ruby image from Docker Hub
FROM ruby:3.2.2

# Install system dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    nodejs \
    postgresql-client \
    poppler-utils

# Set the working directory in the container
WORKDIR /really

# Copy the Gemfile and Gemfile.lock into the container
COPY Gemfile Gemfile.lock ./

# Copy the rest of the application code into the container
COPY . .

# Install dependencies using Bundler
RUN bundle install

# # Add a script to be executed every time the container starts.
EXPOSE 3000

# Command to run the application
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
