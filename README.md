# library-management
## Getting started
# build the docker
sudo docker-compose -f docker-compose.yml build

# push the docker from local to remote into docker hub
sudo docker tag lms_web:latest jgsram/lms:latest
sudo docker push jgsram/lms:latest

# add the image and remove build docker web before starting the docker
image: jgsram/lms:latest

# stop and remove the docker images if any already
sudo docker ps -a -q |sudo  xargs docker stop
sudo docker ps -a -q | sudo xargs docker rm

# Start the docker
sudo docker-compose -f ./docker-compose.yml up -d --force-recreate

# docker containers
sudo docker ps

# database configuration

# drop the database
sudo docker exec <web-container-id> bundle exec rake db:drop

# create database
sudo docker exec <web-container-id> bundle exec rake db:create

# migrate database
sudo docker exec <web-container-id> bundle exec rake db:migrate

# remove the docker images and start again
