Rails.application.routes.draw do
  # get 'books/create'
  # get 'books/update'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"

  resources :books, only: [:create, :update]
end
