class CreateBooks < ActiveRecord::Migration[7.0]
  def change
    ActiveRecord::Base.connection.execute("CREATE EXTENSION IF NOT EXISTS pg_trgm;")

    enable_extension "pg_trgm"
    enable_extension "uuid-ossp"

    create_table :books, id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
      t.string :name
      t.string :author
      t.string :publisher
      t.text :description
      t.datetime "created_at", precision: 6, default: -> { "now()" }, null: false
      t.datetime "updated_at", precision: 6, default: -> { "now()" }, null: false
    end
  end
end
