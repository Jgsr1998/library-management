class Book < ApplicationRecord
  validates :name, presence: true
  validates :author, presence: true
  validates :publisher, presence: true
  validates :description, presence: true
end
