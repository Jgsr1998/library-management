class BooksController < ApplicationController
  before_action :verify_parameters, only: [:create, :update]

  def create
    book = Book.new(book_params)
    if book.save
      notify_graphql_api(book)
      render json: book, status: :created
    else
      render json: book.errors, status: :unprocessable_entity
    end
  end

  def update
    book = Book.find(params[:id])
    if book.update(book_params)
      notify_graphql_api(book)
      render json: book
    else
      render json: book.errors, status: :unprocessable_entity
    end
  end

  private

  def verify_parameters
    return if params[:book].present? && params[:book][:name].present? && params[:book][:author].present? && params[:book][:publisher].present? && params[:book][:description].present?
    render json: { error: 'Missing required data' }, status: :unprocessable_entity
  end

  def book_params
    params.require(:book).permit(:name, :author, :publisher, :description)
  end

  def notify_graphql_api(book)
    graphql_api_endpoints = Rails.application.config.graphql_api_endpoints

    graphql_api_endpoints.each do |endpoint|
      response = HTTParty.post(endpoint, body: { query: mutation_query(book) }.to_json, headers: { 'Content-Type' => 'application/json' })

      if response.code == 200
        Rails.logger.info("Webhook successfully sent to #{endpoint}")
      else
        Rails.logger.error("Failed to send webhook to #{endpoint}: #{response.code} - #{response.body}")
      end
    end
  end

  def mutation_query(book)
    <<~GQL
      mutation {
        insert_books_one(object: {
          name: "#{book.name}",
          author: "#{book.author}",
          publisher: "#{book.publisher}",
          description: "#{book.description}"
        }) {
          id
        }
      }
    GQL
  end

end
